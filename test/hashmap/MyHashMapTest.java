package hashmap;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

public class MyHashMapTest {
    private CustomHashMap<String, Integer> hashMap;

    @Before
    public void setData() {
        hashMap = new CustomHashMap<>();
    }

    @Test
    public void When_PutAnElementToHashMap_Expect_ContainsMethodToReturnTrue() {
        hashMap.put("Nelly", 22);
        assertTrue(hashMap.containsKey("Nelly"));
    }

    @Test
    public void When_SearchForElementWhichHashMapDoesNotContain_Expect_ContainsMethodToReturnFalse() {
        hashMap.put("Nelly", 22);
        hashMap.put("Mechi", 20);
        hashMap.put("Ivan", 22);
        assertFalse(hashMap.containsKey("Gosho"));
    }

    @Test
    public void When_ContainsEntryForCurrentKey_Expect_GetMethodToReturnItsValue() {
        hashMap.put("Nelly", 22);
        assertEquals(22, hashMap.get("Nelly").intValue());

    }

    @Test
    public void When_RemoveEntryFromHashMap_Expect_ContainsMethodForThatKeyToReturnFalse() {
        hashMap.put("Nelly", 22);
        hashMap.remove("Nelly");
        assertFalse(hashMap.containsKey("Nelly"));
    }

}
