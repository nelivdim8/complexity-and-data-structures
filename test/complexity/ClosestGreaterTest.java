package complexity;

import static org.junit.Assert.assertArrayEquals;

import org.junit.Test;

public class ClosestGreaterTest {

	@Test
	public void When_GivenArrayContainsEqualNumbers_Expect_ToReturnOnlyMinusOnes() {
		int[] array = new int[] { 5, 5, 5, 5, 5 };
		int[] result = new int[] { -1, -1, -1, -1, -1 };
		assertArrayEquals(result, ClosestGreater.closestGreater(array));
	}

	@Test(expected = ArrayIndexOutOfBoundsException.class)
	public void When_GivenArrayIsEmpty_Expect_AnExpectionToBeThrown() {
		int[] array = new int[0];

		ClosestGreater.closestGreater(array);
	}

	@Test
	public void When_GivenArrayIsSortedReverseOrder_Expect_ToReturnOnlyMinusOnes() {
		int[] array = new int[] { 6, 5, 4, 3, 2 };
		int[] result = new int[] { -1, -1, -1, -1, -1 };
		assertArrayEquals(result, ClosestGreater.closestGreater(array));
	}

	@Test
	public void When_GivenArrayIsSortedOrder_Expect_ToReturnNextOfEveryElemendAndLastMinusOne() {
		int[] array = new int[] { 2, 3, 4, 5, 6 };
		int[] result = new int[] { 3, 4, 5, 6, -1 };
		assertArrayEquals(result, ClosestGreater.closestGreater(array));
	}

	@Test
	public void When_GivenArrayContainsGreatestElementAtBeginning_Expect_ToReturnNextOfEveryElemendAndLastMinusOne() {
		int[] array = new int[] { 11, 4, 1, 1, 9, 3, 10 };
		int[] result = new int[] { -1, 9, 9, 9, 10, 10, -1 };
		assertArrayEquals(result, ClosestGreater.closestGreater(array));
	}

	@Test
	public void When_GivenArrayContainsGreatestElementAtEnd_Expect_ToReturnLastMinusOneAndOtherLargestValue() {
		int[] array = new int[] { 8, 7, 6, 11 };
		int[] result = new int[] { 11, 11, 11, -1 };
		assertArrayEquals(result, ClosestGreater.closestGreater(array));
	}

}
