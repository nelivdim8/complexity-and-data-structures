package complexity;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class MinMissingNumberSortedArrayTest {

	@Test
	public void When_ArrayContainsEqualNumbers_Expect_ToReturnMinusOne() {
		int[] array = new int[] { 3, 3, 3, 3, 3 };
		assertEquals(-1, MinMissingNumberSortedArray.findMinMissing(array));
	}

	@Test
	public void When_ArrayContainsUnsortedNumbers_Expect_ToReturnFirstMissingMin() {
		int[] array = new int[] { 2, 2, 3, 4, 5, 7, 8 };
		assertEquals(6, MinMissingNumberSortedArray.findMinMissing(array));
	}

	@Test
	public void When_ArrayIsEmpty_Expect_ToReturnMinusOne() {
		int[] array = new int[0];
		assertEquals(-1, MinMissingNumberSortedArray.findMinMissing(array));
	}

	@Test
	public void When_ArrayContainsUnsortedNumbersAndThereIsNoMissing_Expect_ToReturnMinusOne() {
		int[] array = new int[] { 1, 2, 3, 4, 5, 6, 8, 7, 9, 10 };
		assertEquals(-1, MinMissingNumberSortedArray.findMinMissing(array));
	}

	@Test
	public void When_ArrayContainsElementOutOfBounds_Expect_AnExpectionToBeThrown() {
		int[] array = new int[] { 2, 3, 4, 5, 7, 8, 20 };
		assertEquals(6, MinMissingNumberSortedArray.findMinMissing(array));
	}

}
