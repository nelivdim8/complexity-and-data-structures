package complexity;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class ExpandExpressionTest {

	@Test
	public void When_ExpressionContainsBracketsinsideBrackets_Expect_CorrectExpanding() {
		String expression = "AB3(CB2(AB))CA";
		String expected = "ABCBABABCBABABCBABABCA";
		String expanded = ExpandExpression.expandExpression(expression);
		assertEquals(expected, expanded);
	}

	@Test
	public void When_ExpressionContainsBracketsinsideBracketsInTwoPlaces_Expect_CorrectExpanding() {
		String expression = "AB3(CB2(AB))CA2(PQ3(ML)C)K";
		String expected = "ABCBABABCBABABCBABABCAPQMLMLMLCPQMLMLMLCK";
		String expanded = ExpandExpression.expandExpression(expression);
		assertEquals(expected, expanded);
	}

	@Test
	public void When_ExpressionContainsOneBrackets_Expect_CorrectExpanding() {
		String expression = "AB3(A)CAK";
		String expected = "ABAAACAK";
		String expanded = ExpandExpression.expandExpression(expression);
		assertEquals(expected, expanded);
	}

	@Test
	public void When_ExpressionContainsBracketsAtFront_Expect_CorrectExpanding() {
		String expression = "3(BC2(CG))AB3(A)CAK";
		String expected = "BCCGCGBCCGCGBCCGCGABAAACAK";
		String expanded = ExpandExpression.expandExpression(expression);
		assertEquals(expected, expanded);
	}

	@Test
	public void When_ExpressionContainsBracketsAtEnd_Expect_CorrectExpanding() {
		String expression = "AB3(A)CAK3(BC2(CG))";
		String expected = "ABAAACAKBCCGCGBCCGCGBCCGCG";
		String expanded = ExpandExpression.expandExpression(expression);
		assertEquals(expected, expanded);
	}

	@Test
	public void When_ExpressionContainsOnlyBrackets_Expect_CorrectExpanding() {
		String expression = "2(AB)3(BD)3(CG)";
		String expected = "ABABBDBDBDCGCGCG";
		String expanded = ExpandExpression.expandExpression(expression);
		assertEquals(expected, expanded);
	}

	@Test
	public void When_ExpressionContainsOnlyBracketsInsideBrackets_Expect_CorrectExpanding() {
		String expression = "2(AB3(ML2(PQ)A)B)C";
		String expected = "ABMLPQPQAMLPQPQAMLPQPQABABMLPQPQAMLPQPQAMLPQPQABC";
		String expanded = ExpandExpression.expandExpression(expression);
		assertEquals(expected, expanded);
	}

}
