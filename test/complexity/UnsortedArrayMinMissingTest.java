package complexity;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class UnsortedArrayMinMissingTest {

	@Test
	public void WhenMissingValueIsOfRange_Expect_ActualToMatchExpected() {
		int[] array = new int[] { 10, 2, 1, 3, 3, 5, 8, 5, 4, 4 };
		assertEquals(6, UnsortedArrayMinMissing.minMissing(array));
	}

	@Test
	public void WhenArrayIsEmpty_Expect_ToReturnMinusOne() {
		int[] array = new int[0];
		assertEquals(-1, UnsortedArrayMinMissing.minMissing(array));
	}

	@Test(expected = ArrayIndexOutOfBoundsException.class)
	public void WhenArrayContainsValuesOutOfBounds_Expect_AnExceptionToBeThrown() {
		int[] array = new int[] { 20, 1, 5, 6, 2, 3, 4 };
		UnsortedArrayMinMissing.minMissing(array);

	}

	@Test
	public void WhenArrayValuesAreLessOrEqualToSize_Expect_ActualToMatchExpected() {
		int[] array = new int[] { 3, 1, 5, 6, 2, 3, 4 };
		assertEquals(7, UnsortedArrayMinMissing.minMissing(array));

	}

	@Test
	public void WhenArrayContainsNoMissingValues_Expect_ToReturnMinusOne() {
		int[] array = new int[] { 3, 5, 1, 2, 6, 4, 8, 7, 9, 10 };
		assertEquals(-1, UnsortedArrayMinMissing.minMissing(array));

	}

}
