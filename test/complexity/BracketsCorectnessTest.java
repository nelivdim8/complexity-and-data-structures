package complexity;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class BracketsCorectnessTest {

	@Test
	public void When_BracketsAreNotCorrect_Expect_ToReturnFalse() {
		String exp = "(1(+2)-{4}+[432]";

		assertFalse(BracketsCorectness.bracketCorectness(exp));

	}

	@Test
	public void When_BracketsAreCorrect_Expect_ToReturnTrue() {
		String exp = "(1+2)-{4}+[432]";

		assertTrue(BracketsCorectness.bracketCorectness(exp));

	}

	@Test
	public void When_BracketsAreInterlaced_Expect_ToReturnFalse() {
		String exp = "(1+{2-)4}+";

		assertFalse(BracketsCorectness.bracketCorectness(exp));

	}

	@Test
	public void When_ExpressionContainsNoBrackets_Expect_ToReturnTrue() {
		String exp = "1+2-4+432";

		assertTrue(BracketsCorectness.bracketCorectness(exp));

	}

	@Test
	public void When_ExpressionIsEmptyString_Expect_ToReturnTrue() {
		String exp = "";

		assertTrue(BracketsCorectness.bracketCorectness(exp));

	}

	@Test
	public void When_ExpressionContainsOneBracketOfType_Expect_ToReturnFalse() {
		String exp = "2+(13-}=1";

		assertFalse(BracketsCorectness.bracketCorectness(exp));

	}

	@Test
	public void When_ExpressionOnlyBracketsButCorrect_Expect_ToReturnTrue() {
		String exp = "{}()[][{()}]";

		assertTrue(BracketsCorectness.bracketCorectness(exp));

	}

}
