package complexity;

import static org.junit.Assert.assertEquals;

import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

public class NumberCorrenspondingTheTriangleRuleNLogNTest {

	@Test
	public void When_ArrayWithEqualElementsContainsNumbersThatSatisfyTriangleRule_Expect_ToReturnSetOfTheseNumbers() {
		int[] array = new int[] { 4, 1, 6, 5, 9, 6 };// 1 4 5 6 6 9
		Set<Triangle> result = new HashSet<>();
		result.add(new Triangle(1, 6, 6));
		result.add(new Triangle(4, 5, 6));
		result.add(new Triangle(4, 6, 6));
		result.add(new Triangle(5, 6, 6));
		result.add(new Triangle(4, 6, 9));
		result.add(new Triangle(5, 6, 9));
		result.add(new Triangle(6, 6, 9));
		assertEquals(result, NumbersCorrenspondingTheTriangleRuleNLogN.findTriangleRuleNumbersFastest(array));

	}

	@Test
	public void When_ArrayContainsOnlyEqualElements_Expect_ResultToReturnOneTriangle() {
		int[] array = new int[] { 9, 9, 9, 9, 9 };// 1 4 5 6 6 9
		Set<Triangle> result = new HashSet<>();
		result.add(new Triangle(9, 9, 9));
		assertEquals(result, NumbersCorrenspondingTheTriangleRuleNLogN.findTriangleRuleNumbersFastest(array));

	}

	@Test
	public void When_ArrayContainsLessThanThreeElements_Expect_ResultToReturnEmptySet() {
		int[] array = new int[] { 7, 4 };
		Set<Triangle> result = new HashSet<>();
		assertEquals(result, NumbersCorrenspondingTheTriangleRuleNLogN.findTriangleRuleNumbersFastest(array));

	}

	@Test
	public void When_ArrayWithDifferentElementsContainsNumbersThatSatisfyTriangleRule_Expect_ResultToReturnEmptySet() {
		int[] array = new int[] { 7, 4, 2, 5, 1 };// 1 2 4 5 7
		Set<Triangle> result = new HashSet<>();
		result.add(new Triangle(2, 4, 5));
		result.add(new Triangle(4, 5, 7));

		assertEquals(result, NumbersCorrenspondingTheTriangleRuleNLogN.findTriangleRuleNumbersFastest(array));

	}

	@Test
	public void When_ArrayWithDifferentElementsDoesNotContainNumbersThatSatisfyTriangleRule_Expect_ToReturnEmptySet() {
		int[] array = new int[] { 1, 2, 4, 1 };
		Set<Triangle> result = new HashSet<>();
		assertEquals(result, NumbersCorrenspondingTheTriangleRuleNLogN.findTriangleRuleNumbersFastest(array));

	}

	@Test
	public void When_SetIsNotEmpty_Expect_InputElementsNotToBeRemoved() {
		int[] array = new int[] { 3, 4, 6, 7, 8, 9, 12, 20 };
		Set<Triangle> input = new HashSet<>();
		Set<Triangle> result = new HashSet<>();
		input.add(new Triangle(3, 4, 6));
		result.add(new Triangle(3, 4, 6));
		result.add(new Triangle(3, 7, 8));
		result.add(new Triangle(4, 7, 8));
		result.add(new Triangle(6, 7, 8));

		NumbersCorrenspondingTheTriangleRuleNLogN.addSums(array, 0, 3, 4, input);
		assertEquals(result, input);

	}

	@Test
	public void When_SetIsEmpty_Expect_ResultToBeValidSet() {
		int[] array = new int[] { 3, 4, 6, 7, 8, 9, 12, 20 };
		Set<Triangle> input = new HashSet<>();
		Set<Triangle> result = new HashSet<>();

		result.add(new Triangle(3, 7, 8));
		result.add(new Triangle(4, 7, 8));
		result.add(new Triangle(6, 7, 8));

		NumbersCorrenspondingTheTriangleRuleNLogN.addSums(array, 0, 3, 4, input);
		assertEquals(result, input);

	}

	@Test(expected = ArrayIndexOutOfBoundsException.class)
	public void When_OneOfIndexesIsGreaterThanArrayLength_Expect_ResultToThrowAndException() {
		int[] array = new int[] { 3, 4, 6, 7, 8, 9, 12, 20 };
		Set<Triangle> input = new HashSet<>();
		NumbersCorrenspondingTheTriangleRuleNLogN.addSums(array, 0, 3, 20, input);

	}

	@Test
	public void When_ArrayContainsTwoNumbersWhichWithGivenNumberSatisfyTriangleRule_Expect_ValidSetToBeRetured() {
		int[] array = new int[] { 3, 4, 6, 7, 8, 9, 12, 20 };
		Set<Triangle> input = new HashSet<>();
		Set<Triangle> result = new HashSet<>();
		result.add(new Triangle(3, 6, 7));
		result.add(new Triangle(4, 6, 7));

		NumbersCorrenspondingTheTriangleRuleNLogN.checkSums(array, 3, input);
		assertEquals(result, input);

	}

	@Test
	public void When_ArrayLengthIsLessThanThree_Expect_EmptySetToBeRetured() {
		int[] array = new int[] { 3, 4 };
		Set<Triangle> input = new HashSet<>();
		Set<Triangle> result = new HashSet<>();

		NumbersCorrenspondingTheTriangleRuleNLogN.checkSums(array, 0, input);
		assertEquals(result, input);

	}

	@Test
	public void When_ArrayDoesnNotContainAnyTwoNumbersWhichWithGivenNumberSatisfyTriangleRule_Expect_EmptySetToBeRetured() {
		int[] array = new int[] { 1, 2, 4, 1 };
		Set<Triangle> input = new HashSet<>();
		Set<Triangle> result = new HashSet<>();

		NumbersCorrenspondingTheTriangleRuleNLogN.checkSums(array, 1, input);
		assertEquals(result, input);

	}

	@Test
	public void When_IndexIsLessThanZero_Expect_EmptySetToBeRetured() {
		int[] array = new int[] { 3, 4, 6, 7, 8, 9, 12, 20 };
		Set<Triangle> input = new HashSet<>();
		Set<Triangle> result = new HashSet<>();

		NumbersCorrenspondingTheTriangleRuleNLogN.checkSums(array, -1, input);
		assertEquals(result, input);

	}

}
