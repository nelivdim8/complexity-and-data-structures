package complexity;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

public class NumbersCorrenspondingTheTriangleRuleN2LogNTest {

	@Test
	public void When_ArrayWithEqualElementsContainsNumbersThatSatisfyTriangleRule_Expect_ToReturnSetOfTheseNumbers() {
		int[] array = new int[] { 4, 1, 6, 5, 9, 6 };// 1 4 5 6 6 9
		Set<Triangle> result = new HashSet<>();
		result.add(new Triangle(1, 6, 6));
		result.add(new Triangle(4, 5, 6));
		result.add(new Triangle(4, 6, 6));
		result.add(new Triangle(5, 6, 6));
		result.add(new Triangle(4, 6, 9));
		result.add(new Triangle(5, 6, 9));
		result.add(new Triangle(6, 6, 9));
		assertEquals(result, NumbersCorrenspondingTheTriangleRuleN2LogN.findTriangleRuleNumbersFaster(array));

	}

	@Test
	public void When_ArrayContainsOnlyEqualElements_Expect_ResultToReturnOneTriangle() {
		int[] array = new int[] { 9, 9, 9, 9, 9 };
		Set<Triangle> result = new HashSet<>();
		result.add(new Triangle(9, 9, 9));
		assertEquals(result, NumbersCorrenspondingTheTriangleRuleN2LogN.findTriangleRuleNumbersFaster(array));

	}

	@Test
	public void When_ArrayContainsLessThanThreeElements_Expect_ResultToReturnEmptySet() {
		int[] array = new int[] { 7, 4 };
		Set<Triangle> result = new HashSet<>();
		assertEquals(result, NumbersCorrenspondingTheTriangleRuleN2LogN.findTriangleRuleNumbersFaster(array));

	}

	@Test
	public void When_ArrayWithDifferentElementsContainsNumbersThatSatisfyTriangleRule_Expect_ResultToReturnEmptySet() {
		int[] array = new int[] { 7, 4, 2, 5, 1 };// 1 2 4 5 7
		Set<Triangle> result = new HashSet<>();
		result.add(new Triangle(2, 4, 5));
		result.add(new Triangle(4, 5, 7));

		assertEquals(result, NumbersCorrenspondingTheTriangleRuleN2LogN.findTriangleRuleNumbersFaster(array));

	}

	@Test
	public void When_ArrayWithDifferentElementsDoesNotContainNumbersThatSatisfyTriangleRule_Expect_ToReturnEmptySet() {
		int[] array = new int[] { 1, 2, 4, 1 };
		Set<Triangle> result = new HashSet<>();

		assertEquals(result, NumbersCorrenspondingTheTriangleRuleN2LogN.findTriangleRuleNumbersFaster(array));

	}

	@Test
	public void When_ThereAreElementsWhichWithFirstTwoSatisfyTriangleRuleButArrayIsNotSorted_Expect_ToReturnNotEqualSets() {
		int[] array = new int[] { 4, 2, 8, 6, 7, 3 };
		Set<Triangle> input = new HashSet<>();
		Set<Triangle> result = new HashSet<>();
		result.add(new Triangle(2, 3, 4));
		NumbersCorrenspondingTheTriangleRuleN2LogN.binarySearch(input, array, 0, 1);
		assertNotEquals(result, NumbersCorrenspondingTheTriangleRuleN2LogN.binarySearch(input, array, 0, 1));

	}

	@Test
	public void When_ThereAreElementsWhichWithFirstTwoSatisfyTriangleRuleAndArrayIsSorted_Expect_ToReturnValidSet() {
		int[] array = new int[] { 2, 3, 4, 6, 7, 8 };
		Set<Triangle> input = new HashSet<>();
		Set<Triangle> result = new HashSet<>();
		result.add(new Triangle(2, 3, 4));
		assertEquals(result, NumbersCorrenspondingTheTriangleRuleN2LogN.binarySearch(input, array, 0, 1));

	}

	@Test
	public void When_ThereAreNotElementsWhichWithFirstTwoSatisfyTriangleRule_Expect_ToReturnEmptySet() {
		int[] array = new int[] { 1, 2, 4, 1 };
		Set<Triangle> input = new HashSet<>();
		Set<Triangle> result = new HashSet<>();
		assertEquals(result, NumbersCorrenspondingTheTriangleRuleN2LogN.binarySearch(input, array, 0, 1));

	}

	@Test(expected = ArrayIndexOutOfBoundsException.class)
	public void When_InutIndexesAreNotValid_Expect_ToReturnThrowAnException() {
		int[] array = new int[] { 2, 3, 4, 6, 7, 8 };
		Set<Triangle> input = new HashSet<>();
		Set<Triangle> result = new HashSet<>();
		NumbersCorrenspondingTheTriangleRuleN2LogN.binarySearch(input, array, -1, 10);

	}

	@Test
	public void When_SetIsNotEmptyAndThereAreElementsWhichWithFirstTwoSatisfyTriangleRule_Expect_InputElementsNotToBeRemoved() {
		int[] array = new int[] { 2, 3, 4, 6, 7, 8 };
		Set<Triangle> input = new HashSet<>();
		input.add(new Triangle(2, 3, 4));
		Set<Triangle> result = new HashSet<>();
		result.add(new Triangle(2, 3, 4));
		result.add(new Triangle(3, 4, 6));
		assertEquals(result, NumbersCorrenspondingTheTriangleRuleN2LogN.binarySearch(input, array, 1, 2));

	}

	@Test(expected = ArrayIndexOutOfBoundsException.class)
	public void When_ArrayContainsLessThanThreeElements_Expect_AnExceptionToBeThrown() {
		int[] array = new int[] { 2, 3 };
		Set<Triangle> input = new HashSet<>();
		Set<Triangle> result = new HashSet<>();
		result.add(new Triangle(2, 3, 4));
		result.add(new Triangle(3, 4, 6));
		NumbersCorrenspondingTheTriangleRuleN2LogN.binarySearch(input, array, 1, 2);

	}

	@Test
	public void When_MiddleIsGreaterThanLeftAndIndexIIsGreaterThanJ_Expect_ToReturnValidSet() {
		int[] array = new int[] { 1, 2, 4, 6, 7, 8, 9, 12, 20 };
		Set<Triangle> input = new HashSet<>();
		Set<Triangle> result = new HashSet<>();
		result.add(new Triangle(4, 6, 7));
		result.add(new Triangle(4, 6, 8));
		result.add(new Triangle(4, 6, 9));
		NumbersCorrenspondingTheTriangleRuleN2LogN.add(4, 6, 2, 3, array, input);
		assertEquals(result, input);

	}

	@Test
	public void When_MiddleIsSmallerThanLeftAndIndexIIsGreaterThanJ_Expect_ToReturnEmptySet() {
		int[] array = new int[] { 1, 2, 4, 6, 7, 8, 9, 12, 20 };
		Set<Triangle> input = new HashSet<>();
		NumbersCorrenspondingTheTriangleRuleN2LogN.add(6, 4, 2, 3, array, input);
		assertEquals(0, input.size());

	}

	@Test
	public void When_MiddleIsGreaterThanLeftAndIndexIIsSmallerThanJ_Expect_ToReturnDifferentSet() {
		int[] array = new int[] { 1, 2, 4, 6, 7, 8, 9, 12, 20 };
		Set<Triangle> input = new HashSet<>();
		Set<Triangle> result = new HashSet<>();
		result.add(new Triangle(4, 6, 7));
		result.add(new Triangle(4, 6, 8));
		result.add(new Triangle(4, 6, 9));
		NumbersCorrenspondingTheTriangleRuleN2LogN.add(4, 6, 3, 2, array, input);
		assertNotEquals(result, input);

	}

	@Test
	public void When_SetIsNotEmpty_Expect_InputElementsNotToBeRemoved() {
		int[] array = new int[] { 1, 2, 4, 6, 7, 8, 9, 12, 20 };
		Set<Triangle> input = new HashSet<>();
		Set<Triangle> result = new HashSet<>();
		input.add(new Triangle(4, 6, 7));
		input.add(new Triangle(4, 6, 8));
		input.add(new Triangle(4, 6, 9));
		result.add(new Triangle(4, 6, 7));
		result.add(new Triangle(4, 6, 8));
		result.add(new Triangle(4, 6, 9));
		result.add(new Triangle(6, 7, 8));
		result.add(new Triangle(6, 7, 9));
		NumbersCorrenspondingTheTriangleRuleN2LogN.add(5, 6, 3, 4, array, input);
		assertEquals(result, input);

	}
}
