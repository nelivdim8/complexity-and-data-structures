package complexity;

import static org.junit.Assert.assertEquals;

import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

public class NumbersCorrenspondingTheTriangleRuleCubicTest {

	@Test
	public void When_ArrayWithEqualElementsContainsNumbersThatSatisfyTriangleRule_Expect_ToReturnSetOfTheseNumbers() {
		int[] array = new int[] { 4, 1, 6, 5, 9, 6 };// 1 4 5 6 6 9
		Set<Triangle> result = new HashSet<>();
		result.add(new Triangle(1, 6, 6));
		result.add(new Triangle(4, 5, 6));
		result.add(new Triangle(4, 6, 6));
		result.add(new Triangle(5, 6, 6));
		result.add(new Triangle(4, 6, 9));
		result.add(new Triangle(5, 6, 9));
		result.add(new Triangle(6, 6, 9));
		assertEquals(result, NumbersCorrenspondingTheTriangleRuleCubic.findTriangleRuleNumbers(array));

	}

	@Test
	public void When_ArrayContainsOnlyEqualElements_Expect_ResultToReturnOneTriangle() {
		int[] array = new int[] { 9, 9, 9, 9, 9 };
		Set<Triangle> result = new HashSet<>();
		result.add(new Triangle(9, 9, 9));
		assertEquals(result, NumbersCorrenspondingTheTriangleRuleCubic.findTriangleRuleNumbers(array));

	}

	@Test
	public void When_ArrayContainsLessThanThreeElements_Expect_ResultToReturnEmptySet() {
		int[] array = new int[] { 7, 4 };
		Set<Triangle> result = new HashSet<>();
		assertEquals(result, NumbersCorrenspondingTheTriangleRuleCubic.findTriangleRuleNumbers(array));

	}

	@Test
	public void When_ArrayWithDifferentElementsContainsNumbersThatSatisfyTriangleRule_Expect_ResultToReturnEmptySet() {
		int[] array = new int[] { 7, 4, 2, 5, 1 };// 1 2 4 5 7
		Set<Triangle> result = new HashSet<>();
		result.add(new Triangle(2, 4, 5));
		result.add(new Triangle(4, 5, 7));

		assertEquals(result, NumbersCorrenspondingTheTriangleRuleCubic.findTriangleRuleNumbers(array));

	}

	@Test
	public void When_ArrayWithDifferentElementsDoesNotContainNumbersThatSatisfyTriangleRule_Expect_ToReturnEmptySet() {
		int[] array = new int[] { 1, 2, 4, 1 };
		Set<Triangle> result = new HashSet<>();

		assertEquals(result, NumbersCorrenspondingTheTriangleRuleCubic.findTriangleRuleNumbers(array));

	}

}
