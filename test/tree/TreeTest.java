package tree;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class TreeTest {
    private Tree<Integer> tree;

    @Before
    public void setData() {
        tree = new Tree<>();
    }

    @Test
    public void When_InsertNode_Expect_ElementToBeAddedAtCorrectPlace() {
        tree.insert(5);
        tree.insert(3);
        tree.insert(6);
        tree.insert(7);
        tree.insert(4);
        tree.insert(2);
        tree.insert(1);
        List<Integer> expected = Arrays.asList(new Integer[] { 1, 2, 3, 4, 5, 6, 7 });

        List<Integer> actual = tree.inorderList();
        assertEquals(expected, actual);

    }

    @Test
    public void When_RemoveMiddleNode_Expect_RemovingOnlyTheCurrentElement() {
        tree.insert(5);
        tree.insert(3);
        tree.insert(6);
        tree.insert(7);
        tree.insert(4);
        tree.insert(2);
        tree.insert(1);

        tree.remove(5);
        List<Integer> expected = Arrays.asList(new Integer[] { 1, 2, 3, 4, 6, 7 });

        List<Integer> actual = tree.inorderList();
        assertEquals(expected, actual);

    }

    @Test
    public void When_RemoveLeaf_Expect_RemovingOnlyTheCurrentElement() {
        tree.insert(5);
        tree.insert(3);
        tree.insert(6);
        tree.insert(7);
        tree.insert(4);
        tree.insert(2);
        tree.insert(1);
        tree.remove(1);
        List<Integer> expected = Arrays.asList(new Integer[] { 2, 3, 4, 5, 6, 7 });

        List<Integer> actual = tree.inorderList();
        assertEquals(expected, actual);

    }

    @Test
    public void When_SearchForElement_Expect_ToReturnElementWithSearchedValue() {
        tree.insert(5);
        tree.insert(3);
        tree.insert(6);
        tree.insert(7);
        tree.insert(4);
        tree.insert(2);
        tree.insert(1);
        assertEquals(5, tree.getKthElement(5).intValue());
    }

    @Test
    public void When_SearchForElementAtFirstPosition_Expect_ResultToMatchExpected() {
        tree.insert(5);
        tree.insert(3);
        tree.insert(6);
        tree.insert(7);
        tree.insert(4);
        tree.insert(2);
        tree.insert(1);
        assertEquals(1, tree.getKthElement(1).intValue());

    }

    @Test
    public void When_SearchForElementAtLastPosition_Expect_ResultToMatchExpected() {
        tree.insert(5);
        tree.insert(3);
        tree.insert(6);
        tree.insert(7);
        tree.insert(4);
        tree.insert(2);
        tree.insert(1);
        assertEquals(7, tree.getKthElement(7).intValue());

    }

    @Test
    public void When_SearchForElemenWhichIsRoot_Expect_ResultToMatchExpected() {
        tree.insert(5);
        tree.insert(3);
        tree.insert(6);
        tree.insert(7);
        tree.insert(4);
        tree.insert(2);
        tree.insert(1);
        assertEquals(5, tree.getKthElement(5).intValue());

    }

    @Test
    public void When_AddElementsToTree_Expect_CorrectInorderTraversal() {
        tree.insert(10);
        tree.insert(3);
        tree.insert(6);
        tree.insert(17);
        tree.insert(4);
        tree.insert(8);
        tree.insert(12);
        tree.insert(18);
        List<Integer> expected = Arrays.asList(new Integer[] { 3, 4, 6, 8, 10, 12, 17, 18 });

        List<Integer> actual = tree.inorderList();
        assertEquals(expected, actual);
    }

}
