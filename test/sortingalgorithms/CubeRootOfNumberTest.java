package sortingalgorithms;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class CubeRootOfNumberTest {

	@Test
	public void When_GivenInputIsPositiveAndHasAPerfectSquare_Expect_ToReturnCorrectSquare() {

		assertEquals(3.0, CubeRootOfNumber.findCubeRoot(27), 0.0001);
	}

	@Test
	public void When_GivenInputIsPositiveAndHasNotAPerfectSquare_Expect_ToReturnCorrectSquare() {
		assertEquals(1.70997, CubeRootOfNumber.findCubeRoot(5), 0.0001);
	}

	@Test
	public void When_GivenInputIsNegativeAndHasAPerfectSquare_Expect_ToReturnCorrectSquare() {
		assertEquals(-3.0, CubeRootOfNumber.findCubeRoot(-27), 0.0001);
	}

	@Test
	public void When_GivenInputIsNegativeAndHasNotAPerfectSquare_Expect_ToReturnCorrectSquare() {
		assertEquals(-1.70997, CubeRootOfNumber.findCubeRoot(-5), 0.0001);
	}

	@Test
	public void When_GivenInputHasMoreThanFourNumbersAfterDecimalPoint_Expect_ToRemainOnlyThree() {
		assertEquals(1.012, CubeRootOfNumber.nDigitsPrecision(1.01233, 3), 0.0);
	}

	@Test
	public void When_GivenInputHasLessThanFourNumbersAfterDecimalPoint_Expect_ToReturnTheSameNumber() {
		assertEquals(1.012, CubeRootOfNumber.nDigitsPrecision(1.012, 3), 0.0);
	}

	@Test
	public void When_GivenInputHasLessThanThreeNumbersAfterAndLessThanThreeBeforeDecimalPoint_Expect_ToReturnTheSameNumber() {
		assertEquals(11.01, CubeRootOfNumber.nDigitsPrecision(11.01, 3), 0.0);
	}

	@Test
	public void When_GivenInputHasMoreThanFiveNumbersAfterDecimalPoint_Expect_ToRemainOnlyFive() {
		assertEquals(11.01123, CubeRootOfNumber.nDigitsPrecision(11.01123456, 5), 0.0);
	}

}
