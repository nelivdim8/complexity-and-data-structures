package sortingalgorithms;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class BinarySearchTest {

	@Test
	public void When_ArrayContainsOneElementEqualToKey_Expect_ToReturnCorrectIndex() {
		int[] array = new int[] { -23, -7, 2, 3, 5, 6, 11, 21, 75 };
		assertEquals(4, BinarySearch.binarySearch(array, 5));

	}

	@Test
	public void When_ArrayContainsConsecutiveNumbersEqualToKey_Expect_ToReturnFirstMiddleOccurence() {
		int[] array = new int[] { -23, 2, 2, 2, 5, 6, 11, 21, 75 };
		assertEquals(1, BinarySearch.binarySearch(array, 2));

	}

	@Test
	public void When_ArrayDoesNotContainTheKey_Expect_ToReturnMinusOne() {
		int[] array = new int[] { -23, 2, 2, 2, 5, 6, 11, 21, 75 };
		assertEquals(-1, BinarySearch.binarySearch(array, 17));

	}

	@Test
	public void When_ArrayLengthIsZero_Expect_ToReturnMinusOne() {
		int[] array = new int[0];
		assertEquals(-1, BinarySearch.binarySearch(array, 17));

	}

	@Test
	public void When_ArrayContainsKeyAtLastPosition_Expect_ToReturnCorrectIndex() {
		int[] array = new int[] { -23, 2, 2, 2, 5, 6, 11, 21, 75 };
		assertEquals(8, BinarySearch.binarySearch(array, 75));

	}

	@Test
	public void When_ArrayContainsKeyAtFirstPosition_Expect_ToReturnCorrectIndex() {
		int[] array = new int[] { -23, 2, 2, 2, 5, 6, 11, 21, 75 };
		assertEquals(0, BinarySearch.binarySearch(array, -23));

	}

	@Test
	public void When_ArrayHasLengthOneAndElementIsEqualToKey_Expect_ToReturnCorrectIndex() {
		int[] array = new int[] { 5 };
		assertEquals(0, BinarySearch.binarySearch(array, 5));

	}

	@Test
	public void When_ArrayHasLengthOneAndElementIsNotEqualToKey_Expect_ToReturnCorrectMinusOne() {
		int[] array = new int[] { 6 };
		assertEquals(-1, BinarySearch.binarySearch(array, 5));

	}
}
