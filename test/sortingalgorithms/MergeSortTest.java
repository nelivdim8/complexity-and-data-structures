package sortingalgorithms;

import static org.junit.Assert.assertArrayEquals;

import org.junit.Test;

public class MergeSortTest {

	@Test
	public void When_ArrayContainsPositiveAndNegativeNumbers_Expect_CorrectSorting() {
		int[] array = new int[] { 2, 5, 1, -7, 11, -23, 34, 21, 75 };
		int[] sorted = new int[] { -23, -7, 1, 2, 5, 11, 21, 34, 75 };
		MergeSort.mergeSort(array, 0, array.length - 1);
		assertArrayEquals(sorted, array);
	}

	@Test
	public void When_ArrayHasLengthOne_Expect_TheSameArray() {
		int[] array = new int[] { 2 };
		int[] sorted = new int[] { 2 };
		MergeSort.mergeSort(array, 0, array.length - 1);
		assertArrayEquals(sorted, array);
	}

	@Test
	public void When_ArrayIsSortedReveseOrder_Expect_CorrectSorting() {
		int[] array = new int[] { 75, 5, 1, -7, 11, -23, 34, 21, 75 };
		int[] sorted = new int[] { -23, -7, 1, 5, 11, 21, 34, 75, 75 };
		MergeSort.mergeSort(array, 0, array.length - 1);
		assertArrayEquals(sorted, array);
	}

	@Test
	public void When_ArrayIsEmpty_Expect_ToReturnEmptyArray() {
		int[] array = new int[0];
		int[] sorted = new int[0];
		MergeSort.mergeSort(array, 0, array.length - 1);
		assertArrayEquals(sorted, array);
	}

	@Test
	public void When_ArrayIsAlreadySorted_Expect_TheSameArray() {
		int[] array = new int[] { 2, 3, 4, 5, 6, 7, 8 };
		int[] sorted = new int[] { 2, 3, 4, 5, 6, 7, 8 };
		MergeSort.mergeSort(array, 0, array.length - 1);
		assertArrayEquals(sorted, array);
	}

	@Test
	public void When_ArraysAreSameLength_Expect_CorrectMerging() {
		int[] array = new int[] { 2, 5, 1, 3, 6, 4, 7, 0 };
		int[] result = new int[] { 1, 2, 3, 5, 6, 4, 7, 0 };
		MergeSort.merge(0, 1, 3, array);
		assertArrayEquals(result, array);

	}

	@Test
	public void When_ArraysAreDifferentLength_Expect_CorrectMerging() {
		int[] array = new int[] { 1, 2, 5, 3, 6, 4, 7, 0 };
		int[] result = new int[] { 1, 2, 3, 5, 6, 4, 7, 0 };
		MergeSort.merge(0, 2, 4, array);
		assertArrayEquals(result, array);

	}

	@Test
	public void When_ArraysHasLengthOne_Expect_CorrectMerging() {
		int[] array = new int[] { 1, 2 };
		int[] result = new int[] { 1, 2 };
		MergeSort.merge(0, 0, 1, array);
		assertArrayEquals(result, array);

	}

	@Test
	public void When_FirstArrayHasGreaterValues_Expect_CorrectMerging() {
		int[] array = new int[] { 5, 6, 1, 3, 6, 4, 7, 0 };
		int[] result = new int[] { 1, 3, 5, 6, 6, 4, 7, 0 };
		MergeSort.merge(0, 1, 3, array);
		assertArrayEquals(result, array);

	}

	@Test
	public void When_FirstArrayHasSmallerValues_Expect_CorrectMerging() {
		int[] array = new int[] { 1, 3, 5, 6, 6, 4, 7, 0 };
		int[] result = new int[] { 1, 3, 5, 6, 6, 4, 7, 0 };
		MergeSort.merge(0, 1, 3, array);
		assertArrayEquals(result, array);

	}

	@Test
	public void When_FirstArrayHasFirstSmallerSecondGreaterValueThatSecond_Expect_CorrectMerging() {
		int[] array = new int[] { 6, 9, 7, 8, 6, 4, 7, 0 };
		int[] result = new int[] { 6, 7, 8, 9, 6, 4, 7, 0 };
		MergeSort.merge(0, 1, 3, array);
		assertArrayEquals(result, array);

	}

	@Test
	public void When_FirstArrayHasFirstGreaterSecondSmallerValueThatSecond_Expect_CorrectMerging() {
		int[] array = new int[] { 7, 9, 6, 8, 6, 4, 7, 0 };
		int[] result = new int[] { 6, 7, 8, 9, 6, 4, 7, 0 };
		MergeSort.merge(0, 1, 3, array);
		assertArrayEquals(result, array);

	}

}
