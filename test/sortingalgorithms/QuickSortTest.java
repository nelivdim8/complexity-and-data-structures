package sortingalgorithms;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.mockito.Mockito;

public class QuickSortTest {

	@Test
	public void When_ArrayContainsPositiveAndNegativeNumbers_Expect_CorrectSorting() {
		int[] array = new int[] { 2, 5, 1, -7, 11, -23, 34, 21, 75 };
		int[] sorted = new int[] { -23, -7, 1, 2, 5, 11, 21, 34, 75 };
		QuickSort.quickSort(array, 0, array.length - 1);
		assertArrayEquals(sorted, array);
	}

	@Test
	public void When_ArrayHasLengthOne_Expect_TheSameArray() {
		int[] array = new int[] { 2 };
		int[] sorted = new int[] { 2 };
		QuickSort.quickSort(array, 0, array.length - 1);
		assertArrayEquals(sorted, array);
	}

	@Test
	public void When_ArrayIsSortedReveseOrder_Expect_CorrectSorting() {
		int[] array = new int[] { 75, 5, 1, -7, 11, -23, 34, 21, 75 };
		int[] sorted = new int[] { -23, -7, 1, 5, 11, 21, 34, 75, 75 };
		QuickSort.quickSort(array, 0, array.length - 1);
		assertArrayEquals(sorted, array);
	}

	@Test
	public void When_ArrayIsEmpty_Expect_ToReturnEmptyArray() {
		int[] array = new int[0];
		int[] sorted = new int[0];
		QuickSort.quickSort(array, 0, array.length - 1);
		assertArrayEquals(sorted, array);
	}

	@Test
	public void When_ArrayIsAlreadySorted_Expect_TheSameArray() {
		int[] array = new int[] { 2, 3, 4, 5, 6, 7, 8 };
		int[] sorted = new int[] { 2, 3, 4, 5, 6, 7, 8 };
		QuickSort.quickSort(array, 0, array.length - 1);
		assertArrayEquals(sorted, array);
	}

	@Test
	public void When_PivotIsGeneratedInTheMiddle_Expect_CorrectDistributingAndPartitionIndexValue() {
		RandomGenerator rand = Mockito.mock(RandomGenerator.class);
		QuickSort.setRand(rand);
		when(rand.generateNumber(0, 7)).thenReturn(4);
		int[] array = new int[] { 8, 2, 7, 6, 5, 3, 4 };
		int[] result = new int[] { 2, 4, 3, 5, 8, 7, 6 };
		int partitionIndex = QuickSort.partition(array, 0, array.length - 1);
		assertArrayEquals(result, array);
		assertEquals(3, partitionIndex);
		QuickSort.setRand(new RandomGenerator());
	}

	@Test
	public void When_PivotIsGeneratedInTheBeginning_Expect_CorrectDistributingAndPartitionIndexValue() {
		RandomGenerator rand = Mockito.mock(RandomGenerator.class);
		QuickSort.setRand(rand);
		when(rand.generateNumber(0, 7)).thenReturn(0);
		int[] array = new int[] { 8, 2, 7, 6, 5, 3, 4 };
		int[] result = new int[] { 4, 2, 7, 6, 5, 3, 8 };
		int partitionIndex = QuickSort.partition(array, 0, array.length - 1);
		assertArrayEquals(result, array);
		assertEquals(6, partitionIndex);
		QuickSort.setRand(new RandomGenerator());
	}

	@Test
	public void When_PivotIsGeneratedInTheEnd_Expect_CorrectDistributingAndPartitionIndexValue() {
		RandomGenerator rand = Mockito.mock(RandomGenerator.class);
		QuickSort.setRand(rand);
		when(rand.generateNumber(0, 7)).thenReturn(6);
		int[] array = new int[] { 8, 2, 7, 6, 5, 3, 4 };
		int[] result = new int[] { 2, 3, 4, 6, 5, 8, 7 };
		int partitionIndex = QuickSort.partition(array, 0, array.length - 1);
		assertArrayEquals(result, array);
		assertEquals(2, partitionIndex);
		QuickSort.setRand(new RandomGenerator());
	}

	@Test
	public void When_PivotIsGeneratedInTheEndAndArrayIsSortedReverseOrder_Expect_CorrectDistributingAndPartitionIndexValue() {
		RandomGenerator rand = Mockito.mock(RandomGenerator.class);
		QuickSort.setRand(rand);
		when(rand.generateNumber(0, 7)).thenReturn(6);
		int[] array = new int[] { 8, 7, 6, 5, 2, 3, 4 };
		int[] result = new int[] { 2, 3, 4, 5, 8, 7, 6 };
		int partitionIndex = QuickSort.partition(array, 0, array.length - 1);
		assertArrayEquals(result, array);
		assertEquals(2, partitionIndex);
		QuickSort.setRand(new RandomGenerator());
	}

	@Test
	public void When_PivotIsGeneratedInTheMiddleAndArrayIsSortedReverseOrder_Expect_CorrectDistributingAndPartitionIndexValue() {
		RandomGenerator rand = Mockito.mock(RandomGenerator.class);
		QuickSort.setRand(rand);
		when(rand.generateNumber(0, 7)).thenReturn(3);
		int[] array = new int[] { 8, 7, 6, 5, 2, 3, 4 };
		int[] result = new int[] { 4, 2, 3, 5, 7, 6, 8 };
		int partitionIndex = QuickSort.partition(array, 0, array.length - 1);
		assertArrayEquals(result, array);
		assertEquals(3, partitionIndex);
		QuickSort.setRand(new RandomGenerator());
	}

	@Test
	public void When_PivotIsGeneratedInTheBeginningAndArrayIsSortedReverseOrder_Expect_CorrectDistributingAndPartitionIndexValue() {
		RandomGenerator rand = Mockito.mock(RandomGenerator.class);
		QuickSort.setRand(rand);
		when(rand.generateNumber(0, 7)).thenReturn(0);
		int[] array = new int[] { 8, 7, 6, 5, 2, 3, 4 };
		int[] result = new int[] { 4, 7, 6, 5, 2, 3, 8 };
		int partitionIndex = QuickSort.partition(array, 0, array.length - 1);
		assertArrayEquals(result, array);
		assertEquals(6, partitionIndex);
		QuickSort.setRand(new RandomGenerator());
	}

	@Test
	public void When_ArrayIsFilledWithOneValue_Expect_CorrectDistributingAndPartitionIndexValue() {
		RandomGenerator rand = Mockito.mock(RandomGenerator.class);
		QuickSort.setRand(rand);
		when(rand.generateNumber(0, 7)).thenReturn(1);
		int[] array = new int[] { 5, 5, 5, 5 };
		int[] result = new int[] { 5, 5, 5, 5 };
		int partitionIndex = QuickSort.partition(array, 0, array.length - 1);
		assertArrayEquals(result, array);
		assertEquals(3, partitionIndex);
		QuickSort.setRand(new RandomGenerator());
	}

	@Test
	public void When_ArrayIsFilledWithTwoEqualValuesAndAnotheSmaller_Expect_CorrectDistributingAndPartitionIndexValue() {
		RandomGenerator rand = Mockito.mock(RandomGenerator.class);
		QuickSort.setRand(rand);
		when(rand.generateNumber(0, 7)).thenReturn(0);
		int[] array = new int[] { 5, 3, 5 };
		int[] result = new int[] { 5, 3, 5 };
		int partitionIndex = QuickSort.partition(array, 0, array.length - 1);
		assertArrayEquals(result, array);
		assertEquals(2, partitionIndex);
		QuickSort.setRand(new RandomGenerator());
	}

	@Test
	public void When_ArrayIsFilledWithTwoEqualValuesAndAnotheGreater_Expect_CorrectDistributingAndPartitionIndexValue() {
		RandomGenerator rand = Mockito.mock(RandomGenerator.class);
		QuickSort.setRand(rand);
		when(rand.generateNumber(0, 7)).thenReturn(0);
		int[] array = new int[] { 5, 9, 5 };
		int[] result = new int[] { 5, 5, 9 };
		int partitionIndex = QuickSort.partition(array, 0, array.length - 1);
		assertArrayEquals(result, array);
		assertEquals(1, partitionIndex);
		QuickSort.setRand(new RandomGenerator());
	}

}
