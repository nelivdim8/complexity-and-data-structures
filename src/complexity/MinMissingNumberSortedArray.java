package complexity;

public class MinMissingNumberSortedArray {
	public static int findMinMissing(int[] array) {

		for (int i = 0; i < array.length - 1; i++) {
			if (array[i + 1] - array[i] > 1) {
				return array[i] + 1;
			}
		}
		return -1;
	}
}
