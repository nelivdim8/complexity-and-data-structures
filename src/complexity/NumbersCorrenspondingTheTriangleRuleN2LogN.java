package complexity;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class NumbersCorrenspondingTheTriangleRuleN2LogN {

	public static Set<Triangle> findTriangleRuleNumbersFaster(int[] array) {
		Set<Triangle> result = new HashSet<>();
		if (array.length < 3) {
			return result;
		}

		Arrays.sort(array);

		for (int i = 0; i < array.length - 1; i++) {
			for (int j = i + 1; j < array.length; j++) {
				binarySearch(result, array, i, j);
			}
		}
		return result;
	}

	public static Set<Triangle> binarySearch(Set<Triangle> elements, int[] array, int i, int j) {
		int sum = array[i] + array[j];
		int left = j + 1;
		int right = array.length - 1;
		int diff = Math.abs(array[i] - array[j]);
		while (left <= right) {
			int middle = (left + right) / 2;
			if (array[middle] >= sum) {
				right = middle - 1;

			} else {
				if (array[middle] > diff) {
					add(left, middle, i, j, array, elements);
				}
				left = middle + 1;
			}
		}
		return elements;

	}

	public static void add(int left, int middle, int i, int j, int[] array, Set<Triangle> result) {
		for (int j1 = left; j1 <= middle; j1++) {

			result.add(new Triangle(array[i], array[j], array[j1]));

		}

	}

}
