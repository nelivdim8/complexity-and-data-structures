package complexity;

public class Triangle {
	private int first;
	private int second;
	private int third;

	public Triangle(int a, int b, int c) {
		this.first = a;
		this.second = b;
		this.third = c;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + first;
		result = prime * result + second;
		result = prime * result + third;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Triangle other = (Triangle) obj;
		if (first != other.first)
			return false;
		if (second != other.second)
			return false;
		if (third != other.third)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Triangle [first=" + first + ", second=" + second + ", third=" + third + "]";
	}

}
