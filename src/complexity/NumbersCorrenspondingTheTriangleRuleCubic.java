package complexity;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class NumbersCorrenspondingTheTriangleRuleCubic {
	public static Set<Triangle> findTriangleRuleNumbers(int[] array) {
		Set<Triangle> result = new HashSet<>();
		if (array.length < 3) {
			return result;
		}
		Arrays.sort(array);
		for (int i = 0; i < array.length - 2; i++) {
			for (int j = i + 1; j < array.length - 1; j++) {
				for (int k = j + 1; k < array.length; k++) {
					if (array[i] + array[j] > array[k] && array[i] + array[k] > array[j]
							&& array[k] + array[j] > array[i]) {

						result.add(new Triangle(array[i], array[j], array[k]));

					}

				}
			}
		}
		return result;

	}

}
