package complexity;

public class ClosestGreater {
	public static int[] closestGreater(int[] array) {
		int[] result = new int[array.length];
		result[array.length - 1] = -1;
		for (int i = array.length - 2; i >= 0; i--) {
			result[i] = findClosestGreater(i + 1, array.length - 1, array, i);
		}
		return result;
	}

	public static int findClosestGreater(int from, int to, int[] array, int key) {
		int middle = 0;
		int result = -1;
		if (from < to) {
			middle = (from + to) / 2;
			result = findClosestGreater(from, middle, array, key);
			if (result == -1)
				result = findClosestGreater(middle + 1, to, array, key);

		}
		if (from == to && array[from] > array[key]) {
			result = array[from];
		}

		return result;

	}

}
