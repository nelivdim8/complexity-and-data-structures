package complexity;

public class BracketsCorectness {

	public static boolean bracketCorectness(String expression) {
		StackList stack = new StackList();
		char[] elements = expression.toCharArray();
		for (int i = 0; i < elements.length; i++) {
			if (elements[i] == '(' || elements[i] == '{' || elements[i] == '[') {
				stack.push(elements[i]);
			}
			if (stack.size() > 0) {
				if (elements[i] == ']') {
					if ((char) stack.peek() == '[') {
						stack.pop();
					} else {
						return false;
					}
				}
				if (elements[i] == '}') {
					if ((char) stack.peek() == '{') {
						stack.pop();
					} else {
						return false;
					}
				}
				if (elements[i] == ')') {
					if ((char) stack.peek() == '(') {
						stack.pop();
					} else {
						return false;
					}
				}

			}
		}

		return stack.empty();

	}
}
