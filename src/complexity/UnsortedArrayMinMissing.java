package complexity;

public class UnsortedArrayMinMissing {
	public static void main(String[] args) {
		int[] array = new int[] { 10, 2, 1, 3, 3, 5, 5, 8, 4, 4 };
		System.out.println(minMissing(array));

	}

	public static int minMissing(int[] array) {
		int[] valuesCount = new int[array.length + 1];
		for (int i = 0; i < array.length; i++) {
			valuesCount[array[i]]++;
		}
		for (int i = 1; i < valuesCount.length; i++) {
			if (valuesCount[i] == 0) {
				return i;
			}

		}
		return -1;

	}
}
