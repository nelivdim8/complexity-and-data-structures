package complexity;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class NumbersCorrenspondingTheTriangleRuleNLogN {

	public static Set<Triangle> findTriangleRuleNumbersFastest(int[] array) {
		Set<Triangle> result = new HashSet<>();
		if (array.length < 3) {
			return result;
		}
		Arrays.sort(array);

		for (int i = 2; i < array.length; i++) {
			checkSums(array, i, result);

		}
		return result;
	}

	public static void checkSums(int[] array, int k, Set<Triangle> result) {
		int i = 0;
		int j = k - 1;
		while (i < j) {
			if (array[i] + array[j] > array[k]) {
				addSums(array, i, j, k, result);
				j--;
			} else {
				i++;
			}
		}
	}

	public static void addSums(int[] array, int i, int j, int k, Set<Triangle> result) {
		for (int j2 = i; j2 < j; j2++) {
			result.add(new Triangle(array[j2], array[j], array[k]));

		}

	}
}
