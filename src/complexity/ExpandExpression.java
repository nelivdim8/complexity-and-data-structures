package complexity;

public class ExpandExpression {
	private static int counter;

	public static String expandExpression(String expression) {
		counter = 0;
		char[] array = expression.toCharArray();
		return expandExpressionHelper(array, 0, 0);
	}

	public static String expandExpressionHelper(char[] array, int number, int index) {
		StringBuilder result = new StringBuilder();

		for (int i = index; i < array.length; i++) {
			if (Character.isLetter(array[i])) {
				result.append(array[i]);
			} else if (Character.isDigit(array[i])) {
				number = Character.getNumericValue(array[i]);

			} else if (array[i] == '(') {
				StringBuilder temp = new StringBuilder(expandExpressionHelper(array, number, i + 1));
				evaluateExpression(temp, number);
				result.append(temp);
				i = counter;

			} else if (array[i] == ')') {

				counter = i;
				return result.toString();

			}

		}

		return result.toString();
	}

	public static StringBuilder evaluateExpression(StringBuilder temp, int number) {
		String value = temp.toString();
		for (int i = 0; i < number - 1; i++) {
			temp.append(value);

		}
		return temp;

	}
}
