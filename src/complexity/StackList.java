package complexity;

import java.util.EmptyStackException;

public class StackList {
	private ListNode node;
	private int numItems;

	public StackList() {
		this.node = null;
		this.numItems = 0;
	}

	public void push(Object ob) {
		this.node = new ListNode(ob, node);
		numItems++;

	}

	public Object pop() throws EmptyStackException {
		if (numItems == 0) {
			throw new EmptyStackException();
		}
		Object removed = node.getData();
		this.numItems--;
		node = node.getNext();
		return removed;

	}

	public Object peek() {
		if (numItems == 0) {
			throw new EmptyStackException();
		}
		return node.getData();
	}

	public int size() {
		return numItems;
	}

	public boolean empty() {
		return numItems == 0;
	}

}
