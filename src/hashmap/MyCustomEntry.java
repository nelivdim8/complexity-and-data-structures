package hashmap;

import hashmap.CustomMap.CustomEntry;

public class MyCustomEntry<K, V> implements CustomEntry<K, V> {
    private K key;
    private V value;

    public MyCustomEntry(K key, V value) {
        super();
        this.key = key;
        this.value = value;
    }

    @Override
    public K getKey() {
        return key;
    }

    @Override
    public V getValue() {
        return value;
    }

    @Override
    public V setValue(Object value) {
        V temp = this.value;
        this.value = (V) value;
        return temp;
    }

    @Override
    public String toString() {
        return "[Key: " + key + ", Value: " + value + "]";
    }

}
