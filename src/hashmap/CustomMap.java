package hashmap;

import java.util.Collection;
import java.util.Set;

public interface CustomMap<K, V> extends Iterable<CustomMap.CustomEntry<K, V>> {
	void put(K key, V value);

	V get(Object value);

	boolean isEmpty();

	int size();

	boolean containsKey(K key);

	boolean containsValue(V value);

	V remove(K key);

	Set<CustomMap.CustomEntry<K, V>> entrySet();

	Set<K> keySet();

	Collection<V> values();

	interface CustomEntry<K, V> {
		K getKey();

		V getValue();

		V setValue(Object value);
	}

}
