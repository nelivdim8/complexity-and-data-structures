package hashmap;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Formatter;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

import hashmap.CustomMap.CustomEntry;

public class FileCompressor {
    public static void main(String[] args) {
        compress(new File("Words.txt"));

    }

    public static List<String> readFile(File filename) {
        List<String> text = new ArrayList<>();
        Scanner input = null;
        try {
            input = new Scanner(filename);
        } catch (FileNotFoundException e) {

            e.printStackTrace();
        }

        while (input.hasNext()) {

            text.add(input.next());
        }

        return text;
    }

    public static CustomHashMap<String, Integer> countWords(List<String> words) {
        CustomHashMap<String, Integer> map = new CustomHashMap<>();

        for (int i = 0; i < words.size(); i++) {

            if (map.containsKey(words.get(i))) {
                map.put(words.get(i), map.get(words.get(i)) + 1);
            } else {
                map.put(words.get(i), 1);
            }

        }

        return map;

    }

    public static List<Integer> getMaxOccurence(CustomHashMap<String, Integer> map) {

        List<Integer> sorted = new ArrayList<>(map.values());
        Collections.sort(sorted, Collections.reverseOrder());
        System.out.println(sorted.toString());
        return sorted;

    }

    public static void compress(File filename) {
        List<String> words = readFile(filename);
        CustomHashMap<String, Integer> map = countWords(words);
        List<Integer> sorted = getMaxOccurence(map);
        boolean[] visited = new boolean[sorted.size()];

        for (Iterator iterator = map.iterator(); iterator.hasNext();) {
            CustomEntry<String, Integer> entry = (CustomEntry<String, Integer>) iterator.next();

            map.put(entry.getKey(), calculateCompressedValue(entry, sorted, visited));

        }
        for (Iterator iterator = map.iterator(); iterator.hasNext();) {
            CustomEntry<String, Integer> entry = (CustomEntry<String, Integer>) iterator.next();
            System.out.println(entry.getKey() + " " + entry.getValue());

        }
        write(convertToDigits(words, map));
    }

    private static int calculateCompressedValue(CustomEntry<String, Integer> entry, List<Integer> sorted,
            boolean[] visited) {

        int index = sorted.indexOf(entry.getValue());
        if (!visited[index]) {
            visited[index] = true;
        } else {
            while (visited[index]) {
                index++;
            }
            visited[index] = true;

        }
        return index;

    }

    public static List<Integer> convertToDigits(List<String> words, CustomHashMap<String, Integer> map) {
        List<Integer> result = new ArrayList<>();
        for (int i = 0; i < words.size(); i++) {
            result.add(map.get(words.get(i)));

        }
        return result;
    }

    public static void write(List<Integer> compressed) {

        Formatter wr;
        try {
            wr = new Formatter(new File("Compressed.txt"));

            for (Integer integer : compressed) {

                wr.format("%d ", integer);

            }
        } catch (FileNotFoundException e) {

            e.printStackTrace();
        } catch (IOException e) {

            e.printStackTrace();
        }

    }

}
