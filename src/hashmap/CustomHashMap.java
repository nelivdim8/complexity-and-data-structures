package hashmap;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class CustomHashMap<K, V> implements CustomMap<K, V> {
    private List<CustomMap.CustomEntry<K, V>>[] entries;
    private final static double LOAD_FACTOR = 0.75;
    private final static int INITIAL_SIZE = 30;
    private int size;

    public CustomHashMap() {
        super();
        this.entries = (LinkedList<CustomMap.CustomEntry<K, V>>[]) new LinkedList[INITIAL_SIZE];
        this.size = 0;
    }

    private int hashString(String key) {
        int hash = 1;
        int base = 29;
        for (int i = 0; i < key.length(); i++) {
            hash = (hash * base + key.charAt(i)) % entries.length;
        }
        return hash;
    }

    private boolean shouldResize() {
        return LOAD_FACTOR * entries.length <= size;

    }

    private void resize(int newLength) {
        size = 0;
        List<CustomMap.CustomEntry<K, V>>[] temp = entries;
        entries = new LinkedList[newLength];
        for (List<CustomMap.CustomEntry<K, V>> list : temp) {
            if (list != null) {
                for (CustomMap.CustomEntry<K, V> entry : list) {
                    put(entry.getKey(), entry.getValue());
                }

            }
        }

    }

    @Override
    public void put(Object key, Object value) {
        if (shouldResize()) {
            resize(entries.length * 2);
        }
        int index = hashString((String) key);
        if (entries[index] == null) {
            entries[index] = new LinkedList<>();

        }
        for (int i = 0; i < entries[index].size(); i++) {
            if (entries[index].get(i).getKey().equals(key)) {
                entries[index].get(i).setValue((V) value);
                return;
            }

        }
        entries[index].add(new MyCustomEntry(key, value));
        size++;
    }

    @Override
    public boolean containsKey(Object key) {
        int index = hashString((String) key);
        if (entries[index] != null) {

            for (int i = 0; i < entries[index].size(); i++) {
                if (entries[index].get(i).getKey().equals(key)) {
                    return true;
                }

            }

        }
        return false;

    }

    @Override
    public V get(Object key) {
        int index = hashString((String) key);
        if (entries[index] != null) {

            for (int i = 0; i < entries[index].size(); i++) {
                if (entries[index].get(i).getKey().equals(key)) {
                    return entries[index].get(i).getValue();
                }

            }

        }
        return null;

    }

    @Override
    public V remove(K key) {
        int index = hashString((String) key);
        int i = 0;
        while (i < entries[index].size()) {
            if (entries[index].get(i).getKey().equals(key)) {
                size--;
                return (V) entries[index].remove(i);
            }
        }
        return null;

    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public Set<CustomEntry<K, V>> entrySet() {

        Set<CustomEntry<K, V>> set = new HashSet<>();
        if (isEmpty()) {
            for (List<CustomEntry<K, V>> list : entries) {
                if (list != null) {
                    for (CustomEntry<K, V> customEntry : list) {
                        set.add(customEntry);
                    }
                }
            }
        }

        return set;
    }

    @Override
    public Set<K> keySet() {
        Set<K> set = new HashSet<>();
        if (!isEmpty()) {
            for (List<CustomEntry<K, V>> list : entries) {
                if (list != null) {
                    for (CustomEntry<K, V> customEntry : list) {
                        set.add(customEntry.getKey());
                    }
                }
            }

        }

        return set;
    }

    @Override
    public Collection<V> values() {
        Collection<V> values = new LinkedList<>();
        if (!isEmpty()) {
            for (List<CustomEntry<K, V>> list : entries) {
                if (list != null) {
                    for (CustomEntry<K, V> customEntry : list) {
                        values.add(customEntry.getValue());
                    }
                }
            }
        }

        return values;
    }

    @Override
    public boolean containsValue(V value) {
        if (!isEmpty()) {
            for (List<CustomEntry<K, V>> list : entries) {
                if (list != null) {
                    for (CustomEntry<K, V> customEntry : list) {
                        if (customEntry.getValue().equals(value)) {
                            return true;
                        }
                    }
                }
            }

        }
        return false;
    }

    @Override
    public Iterator<CustomEntry<K, V>> iterator() {
        return new CustomHashMapIterator(this);
    }

    public class CustomHashMapIterator implements Iterator<CustomEntry<K, V>> {
        private int index;
        private List<CustomMap.CustomEntry<K, V>> elements;

        public CustomHashMapIterator(CustomHashMap<K, V> el) {

            this.index = 0;
            this.elements = getElements(el);

        }

        private List<CustomEntry<K, V>> getElements(CustomHashMap<K, V> el) {
            List<CustomEntry<K, V>> e1 = new LinkedList<>();
            for (List<CustomEntry<K, V>> entry : el.entries) {
                if (entry != null) {
                    for (CustomEntry<K, V> entry2 : entry) {
                        e1.add(entry2);

                    }
                }
            }
            return e1;
        }

        @Override
        public boolean hasNext() {
            return index < elements.size();
        }

        @Override
        public CustomEntry<K, V> next() {

            CustomEntry<K, V> element = elements.get(index);
            index++;
            return element;
        }

    }

}
