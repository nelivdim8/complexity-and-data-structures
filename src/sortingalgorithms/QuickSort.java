package sortingalgorithms;

public class QuickSort {
	private static RandomGenerator rand = new RandomGenerator();

	public static RandomGenerator getRand() {
		return rand;
	}

	public static void setRand(RandomGenerator rand) {
		QuickSort.rand = rand;
	}

	public static void quickSort(int[] array, int start, int end) {
		if (start >= end) {
			return;
		}

		int partition = partition(array, start, end);
		quickSort(array, start, partition - 1);
		quickSort(array, partition + 1, end);

	}

	public static int partition(int[] array, int start, int end) {
		int pivot = rand.generateNumber(start, end - start + 1);
		swap(end, pivot, array);
		return partitionHelper(array, start, end);
	}

	public static int partitionHelper(int[] array, int start, int end) {
		int pivot = array[end];
		int pIndex = start - 1;
		for (int i = start; i <= end; i++) {
			if (array[i] <= pivot) {
				pIndex++;
				swap(i, pIndex, array);

			}

		}

		return pIndex;

	}

	public static void swap(int i, int pIndex, int[] array) {
		int temp = array[i];
		array[i] = array[pIndex];
		array[pIndex] = temp;

	}

}
