package sortingalgorithms;

import java.util.Arrays;

public class MergeSort {
	public static void mergeSort(int[] array, int start, int end) {
		if (start >= end) {
			return;
		}
		int middle = (start + end) / 2;
		mergeSort(array, start, middle);
		mergeSort(array, middle + 1, end);
		merge(start, middle, end, array);

	}

	public static void merge(int start, int middle, int end, int[] array) {
		int[] array1 = Arrays.copyOfRange(array, start, middle + 1);
		int[] array2 = Arrays.copyOfRange(array, middle + 1, end + 1);
		int k1 = 0;
		int k2 = 0;
		int j = start;

		while (k1 < array1.length && k2 < array2.length) {
			if (array1[k1] <= array2[k2]) {
				array[j++] = array1[k1++];

			} else {

				array[j++] = array2[k2++];

			}

		}

		while (k1 < array1.length) {
			array[j++] = array1[k1++];

		}
		while (k2 < array2.length) {
			array[j++] = array2[k2++];

		}

	}

}
