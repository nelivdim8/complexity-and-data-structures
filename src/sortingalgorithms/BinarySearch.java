package sortingalgorithms;

public class BinarySearch {

	public static int binarySearch(int[] array, int key) {
		int left = 0;
		int right = array.length - 1;

		while (left <= right) {
			int middle = (left + right) / 2;
			if (array[middle] == key) {
				return middle;
			} else if (array[middle] > key) {
				right = middle - 1;
			} else {
				left = middle + 1;
			}
		}
		return -1;

	}

}
