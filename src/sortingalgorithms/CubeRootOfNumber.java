package sortingalgorithms;

public class CubeRootOfNumber {
	public static void main(String[] args) {
		System.out.println(findCubeRoot(-5));
	}

	public static double findCubeRoot(double x) {
		double x1 = Math.abs(x);
		double left = -x1;
		double right = x1;
		double middle = 0.0;
		double precision = 0.0009;
		while (left <= right) {
			middle = (left + right) / 2;
			double difference = Math.abs(x1 - middle * middle * middle);
			if (difference <= precision) {
				break;
			} else if (middle * middle * middle < x1) {
				left = middle;
			} else {
				right = middle;
			}
		}
		middle = nDigitsPrecision(middle, 5);
		if (x < 0) {
			middle = -middle;
		}
		return middle;

	}

	public static double nDigitsPrecision(double number, int n) {
		String numberStr = String.valueOf(number);
		int indexOfPoint = numberStr.indexOf('.');
		if (numberStr.length() >= indexOfPoint + n + 1) {
			return Double.parseDouble(numberStr.substring(0, indexOfPoint + n + 1));
		}
		return number;

	}
}
