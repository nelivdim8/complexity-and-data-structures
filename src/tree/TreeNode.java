package tree;

public class TreeNode<T> {
	private T data;
	private TreeNode<T> left;
	private TreeNode<T> right;
	private TreeNode<T> parent;
	private int index;

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public TreeNode(T data, TreeNode<T> left, TreeNode<T> right) {
		super();
		this.data = data;
		this.left = left;
		this.right = right;

	}

	public TreeNode(T data) {
		super();
		this.data = data;
		this.left = null;
		this.right = null;
		this.parent = null;
		this.index = 1;
	}

	public TreeNode(T data, TreeNode<T> parent) {
		super();
		this.data = data;
		this.left = null;
		this.right = null;
		this.parent = parent;
		this.index = 1;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

	public TreeNode<T> getLeft() {
		return left;
	}

	public void setLeft(TreeNode<T> left) {
		this.left = left;
	}

	public TreeNode<T> getRight() {
		return right;
	}

	public void setRight(TreeNode<T> right) {
		this.right = right;
	}

	public TreeNode<T> getParent() {
		return parent;
	}

	public void setParent(TreeNode<T> parent) {
		this.parent = parent;
	}

	@Override
	public String toString() {
		return "TreeNode [data=" + data + "index" + index + "]";
	}

}
