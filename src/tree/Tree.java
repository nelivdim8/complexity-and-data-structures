package tree;

import java.util.LinkedList;
import java.util.List;

public class Tree<T extends Comparable<T>> {
    private TreeNode<T> root;

    public Tree() {
        super();
        this.root = null;
        this.size = 0;
    }

    private int size;

    public int getSize() {
        return size;
    }

    public void insert(T data) {
        if (root == null) {
            this.root = new TreeNode<>(data);
            return;
        }

        insertHelper(root, null, data);

    }

    private void insertHelper(TreeNode<T> node, TreeNode<T> parent, T data) {
        node.setIndex(node.getIndex() + 1);
        if (data.compareTo(node.getData()) < 0) {
            if (node.getLeft() == null) {
                node.setLeft(new TreeNode<>(data, node));
                return;
            }

            insertHelper(node.getLeft(), node, data);
        } else {
            if (node.getRight() == null) {
                node.setRight(new TreeNode<>(data, node));
                return;
            }
            insertHelper(node.getRight(), node, data);
        }

    }

    public boolean contains(T data) {
        return containsHelper(root, data);
    }

    private boolean containsHelper(TreeNode<T> node, T data) {
        if (node == null) {
            return false;
        }
        if (node.getData().equals(data)) {
            return true;
        }
        if (data.compareTo(node.getData()) < 0) {
            return containsHelper(node.getLeft(), data);
        }
        return containsHelper(node.getRight(), data);
    }

    public TreeNode<T> find(T data) {
        return findHelper(root, data);

    }

    private TreeNode<T> findHelper(TreeNode<T> node, T data) {
        if (node.getData().equals(data)) {
            return node;
        }
        node.setIndex(node.getIndex() - 1);
        if (data.compareTo(node.getData()) < 0) {
            return findHelper(node.getLeft(), data);
        }
        return findHelper(node.getRight(), data);
    }

    public T findMin() {
        if (root != null)
            return findMinHelper(root);
        return null;
    }

    public T findMax() {
        if (root != null)
            return findMaxHelper(root);
        return null;
    }

    private T findMaxHelper(TreeNode<T> node) {
        if (node.getRight() != null) {
            return findMaxHelper(node.getRight());
        }
        return node.getData();
    }

    private T findMinHelper(TreeNode<T> node) {
        if (node.getLeft() != null) {
            return findMaxHelper(node.getLeft());
        }
        return node.getData();
    }

    private TreeNode<T> findMaxLeft(TreeNode<T> node) {

        if (node.getRight() == null) {
            return node;
        }
        node.setIndex(node.getIndex() - 1);
        return findMaxLeft(node.getRight());

    }

    private TreeNode<T> findMinRight(TreeNode<T> node) {
        if (node.getLeft() == null) {
            return node;
        }
        node.setIndex(node.getIndex() - 1);
        return findMaxLeft(node.getLeft());

    }

    public T remove(T data) {
        if (root != null) {
            if (this.contains(data)) {

                TreeNode<T> node = find(data);
                TreeNode<T> replacement = null;

                if (node.getLeft() == null && node.getRight() == null) {
                    if (node.getData().compareTo(node.getParent().getData()) < 0) {
                        node.getParent().setLeft(null);

                    } else {
                        node.getParent().setRight(null);
                    }

                } else {
                    if (node.getLeft() == null) {
                        replacement = findMinRight(node.getRight());
                    } else {
                        replacement = findMaxLeft(node.getLeft());
                    }
                    T val = replacement.getData();
                    TreeNode<T> child = null;
                    if (replacement.getLeft() != null) {
                        child = replacement.getLeft();

                    }
                    if (replacement.getRight() != null) {
                        child = replacement.getRight();

                    }
                    if (replacement.getData().compareTo(replacement.getParent().getData()) < 0) {

                        replacement.getParent().setLeft(child);

                    } else {
                        replacement.getParent().setRight(child);
                    }
                    node.setData(val);

                }
                return data;
            }
        }
        return null;

    }

    public void inorderTraversal() {
        inorderTraversalHelper(root);
        System.out.println();
    }

    private void inorderTraversalHelper(TreeNode<T> node) {
        if (node == null) {
            return;
        }
        inorderTraversalHelper(node.getLeft());
        System.out.println(node);
        inorderTraversalHelper(node.getRight());
    }

    public List<T> inorderList() {
        List<T> data = new LinkedList<>();
        inorderListHelper(root, data);
        return data;

    }

    private void inorderListHelper(TreeNode<T> node, List<T> data) {
        if (node == null) {
            return;
        }

        inorderListHelper(node.getLeft(), data);
        data.add(node.getData());
        inorderListHelper(node.getRight(), data);

    }

    public TreeNode<T> lowerBound(T data) {
        return lowerBoundHelper(root, data);
    }

    private TreeNode<T> lowerBoundHelper(TreeNode<T> node, T data) {
        if (node.getData().equals(data)) {
            return node;
        }
        if (data.compareTo(node.getData()) > 0) {
            lowerBoundHelper(node.getRight(), data);
        }
        TreeNode<T> leftNode = lowerBoundHelper(node.getLeft(), data);
        if (leftNode == null) {
            return node;
        }
        return leftNode;
    }

    public T getKthElement(int k) {

        return getKthElementHelper(k, root);
    }

    private T getKthElementHelper(int counter, TreeNode<T> node) {
        if (node.getLeft() != null) {
            if (node.getLeft().getIndex() < counter) {
                counter -= node.getLeft().getIndex();
            } else {
                return getKthElementHelper(counter, node.getLeft());
            }
        }
        if (counter == 1) {
            return node.getData();
        }
        counter--;
        return getKthElementHelper(counter, node.getRight());

    }

}
